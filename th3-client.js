/* Author: Cillian de Róiste <cillian.deroiste@gmail.com>
   License: AGPL
   Repo: https://gitorious.org/th3
   Reference: http://blog.gingertech.net/2009/08/19/jumping-to-time-offsets-in-videos/
*/

var TH3 = {};

TH3.queryString = function () {
    // CMS: http://stackoverflow.com/a/647272
    var result = {}, queryString = location.search.substring(1),
    re = /([^&=]+)=([^&]*)/g, m;

    while (m = re.exec(queryString)) {
	    result[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
    }

    return result;
}();

TH3.sections = window.location.hash
    .replace("#t=", "")
    .split(",");

TH3.parseTime = function (str) {
    /* Support standard time codes
       https://developer.mozilla.org/en/Using_HTML5_audio_and_video#Specifying_playback_range
    */
    if (str == "") { return 0; }

    hms = str.split(":");
    if (hms.length == 3) {
        var hours = parseInt(hms[0]),
        minutes = parseInt(hms[1]),
        seconds = parseInt(hms[2])
        return hours*60*60 + minutes*60 + seconds;
    }

    return parseFloat(str);
}

TH3.toTimeString = function (totalSeconds) {
    var hours = parseInt( totalSeconds / ( 60 * 60 ) ),
        minutes = parseInt( ( totalSeconds - ( hours * 60 * 60 ) ) / 60 ),
        seconds = totalSeconds - ( minutes * 60 );
    return hours + ":" + minutes + ":" + seconds;
}

TH3.prepareMedia = function () {
    TH3.resource.setAttribute("controls", "controls");
    if (TH3.sections.length > 0) {
        TH3.startTime = TH3.parseTime(TH3.sections[0]);
        TH3.resource.addEventListener("loadedmetadata", function () {
            if (TH3.sections.length > 1) {
                TH3.endTime = TH3.parseTime(TH3.sections[1]);
                function pause() {
		            if (TH3.resource.currentTime >= TH3.endTime) {
			            TH3.resource.pause();
                        TH3.resource.removeEventListener("timeupdate", pause, false);
		            }
                }
		        TH3.resource.addEventListener("timeupdate", pause, false);
            }
	        TH3.resource.currentTime = TH3.startTime;
            TH3.resource.play();
        }, false);
    }
}

TH3.searchReddit = function (src) {
    $.getJSON("http://www.reddit.com/search.json?jsonp=?&q=url:" + src, function (searchResults) {
        var comments = searchResults.data ? searchResults.data.children ? searchResults.data.children : [] : [];

        if (comments != []) {
            var enhancedComments = [];
            for (i = 0; i < comments.length; i++) {
                var data = comments[i].data,
                today = new Date(),
                creationDate = new Date(data.created * 1000),
                delta = today.valueOf() - creationDate.valueOf(),
                second = 1000,
                minute = second * 60,
                hour = minute * 60,
                day = hour * 24,
                month = day * 31,
                year = day * 365,
                ago_string = "";

                if (delta < second) {
                    ago_string = delta + " milliseconds";
                }
                else if (delta < minute) {
                    ago_string = parseInt(delta / second) + " seconds";
                }
                else if (delta < hour) {
                    ago_string = parseInt(delta / minute) + " minutes";
                }
                else if (delta < day) {
                    ago_string = parseInt(delta / hour) + " hours";
                }
                else if (delta < month) {
                    ago_string = parseInt(delta / day) + " days";
                }
                else if (delta < year) {
                    ago_string = parseInt(delta / month) + " months";
                }
                else if (delta > year) {
                    ago_string = parseInt(delta / year) + " years";
                }
                data.ago = ago_string + " ago";
                enhancedComments.push(data)
            }
            var resourceComments = Handlebars.templates["resource-comments"]({comments: enhancedComments});
            $(resourceComments).appendTo("#main");
        }
    });
}

if ("audio" in TH3.queryString) {
    TH3.resource = new Audio();
    TH3.resource.src = TH3.queryString["audio"];
    TH3.prepareMedia();
}

if ("video" in TH3.queryString) {
    TH3.resource = document.createElement("video");
    TH3.resource.src = TH3.queryString["video"];
    TH3.prepareMedia();
}


window.onload = function () {
    if (TH3.resource != undefined) {
        var resourceDetails = {
            src: TH3.resource.src,
            startTime: "Start",
            endTime: "End",
        };

        if (TH3.startTime != undefined) {
            resourceDetails["startTime"] = TH3.toTimeString(TH3.startTime);
        }
        if (TH3.endTime != undefined) {
            resourceDetails["endTime"] = TH3.toTimeString(TH3.endTime);
        }

        var details = Handlebars.templates["resource-details"](resourceDetails);
        $("#main").html(details);

        var resource = document.getElementById("resource");
        resource.appendChild(TH3.resource);

        TH3.searchReddit(TH3.resource.src);
    }

};


