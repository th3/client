(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['resource-details'] = template(function (Handlebars,depth0,helpers,partials,data) {
  helpers = helpers || Handlebars.helpers;
  var buffer = "", stack1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;


  buffer += "<div id=\"resource-details\" class=\"box\">\n  <dl>\n    <dt>Source URL:\n    </dt>\n    <dd><a href=\"";
  stack1 = helpers.src || depth0.src;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "src", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\">";
  stack1 = helpers.src || depth0.src;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "src", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a>\n    </dd>\n    <dt>Start time:\n    </dt>\n    <dd>";
  stack1 = helpers.startTime || depth0.startTime;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "startTime", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\n    </dd>\n    <dt>End Time:\n    </dt>\n    <dd>";
  stack1 = helpers.endTime || depth0.endTime;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "endTime", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\n    </dd>\n  </dl>\n</div>\n<div id=\"resource\" class=\"box\">\n</div>";
  return buffer;});
})()