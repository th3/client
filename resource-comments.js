(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['resource-comments'] = template(function (Handlebars,depth0,helpers,partials,data) {
  helpers = helpers || Handlebars.helpers;
  var buffer = "", stack1, stack2, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n  <div class=\"comment\">\n    <span class=\"score\">Score: ";
  stack1 = helpers.score || depth0.score;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "score", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</span>\n    <div class=\"details\">\n      <p class=\"title\">";
  stack1 = helpers.title || depth0.title;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "title", { hash: {} }); }
  buffer += escapeExpression(stack1) + " (";
  stack1 = helpers.domain || depth0.domain;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "domain", { hash: {} }); }
  buffer += escapeExpression(stack1) + ")</p>\n      <p>Submitted ";
  stack1 = helpers.ago || depth0.ago;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ago", { hash: {} }); }
  buffer += escapeExpression(stack1) + " by ";
  stack1 = helpers.author || depth0.author;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "author", { hash: {} }); }
  buffer += escapeExpression(stack1) + " to <a href=\"http://www.reddit.com/r/";
  stack1 = helpers.subreddit || depth0.subreddit;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "subreddit", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\">";
  stack1 = helpers.subreddit || depth0.subreddit;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "subreddit", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a>\n      <a href=\"http://www.reddit.com/";
  stack1 = helpers.permalink || depth0.permalink;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "permalink", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\">";
  stack1 = helpers.num_comments || depth0.num_comments;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "num_comments", { hash: {} }); }
  buffer += escapeExpression(stack1) + " comments</a>\n      </p>\n    </div>\n  </div>\n  ";
  return buffer;}

  buffer += "<div id=\"resource-comments\" class=\"box\">\n  <h2>Reddit</h2>\n  ";
  stack1 = helpers.comments || depth0.comments;
  stack2 = helpers.each;
  tmp1 = self.program(1, program1, data);
  tmp1.hash = {};
  tmp1.fn = tmp1;
  tmp1.inverse = self.noop;
  stack1 = stack2.call(depth0, stack1, tmp1);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</div>";
  return buffer;});
})()